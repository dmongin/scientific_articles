# Decrease of heart rate variability during exercise: an index of cardiorespiratory fitness

This is a gitlab repository for the article "Decrease of heart rate variability during exercise: an index of cardiorespiratory fitness", by Denis Mongin, Clovis Chabert, Manuel Gomez Extremera, Olivier Hue, Delphine Sophie Courvoisier, Pedro Carpena, and Pedro Angel, Bernaola Galvan.

ECG data are avilable at the physionet repository, and in the [physionet_files](./physionet_files) folder.

The preprint of the article is available here: https://doi.org/10.1101/2021.09.23.21263943

- [example.Rmd](example.Rmd) provides few lines of code to plot the data
- [data_management.R](./data_management.R) Loads the data, clean the data, and calculate HRV indices. It output a clean_data.RData file.
- [detrending_doremi.R](./detrending_doremi.R) performs the RR series detrending using the doremi package
- [detrending_local_pol.R](./detrending_local_pol.R) performs the RR series detrending using local polynomial detrending
- [analysis.R](./analysis.R) contains the main analysis script
- [figures_table.R](./figures_table.R) contains the code to produce the figures and tables of the article

