library(data.table)
library(ggplot2)
library(stringr)
library(Hmisc)
library(tableone)
library(officer)
library(flextable)

rm(list = ls())
setwd(dirname(rstudioapi::getActiveDocumentContext()$path))
load("doremi_analysis.RData")
load("clean_data.RData")
load("sensibility_analysis.RData")

article <- "./figure_tables/"
#### descriptive data

ecg_data[,.N,by = ID]$N %>% summary()

# model 1 summary
model_res$model1 %>% summary()

###### figure 2 ##########

y_conv_fact <- doremi_data[ID == 14,(max(power)+100)/max(RR)]
p1 <- ggplot(doremi_data[ID == 12],aes(x = time))+
  geom_line(aes(y = power/y_conv_fact,color = "power"),size = 1)+
  geom_line(aes(y = RR,color = "RR"),size = 1)+
  geom_line(aes(y = RR_doremi,color = "model estimate"),size = 1)+
  geom_line(aes(y = RR - RR_doremi,color = "RR detrend"),size = 1)+
  scale_y_continuous(
    sec.axis = sec_axis(~.*y_conv_fact,name = "Power (W)"),limits = c(-100,1200)
    
  )+
  scale_color_manual(values = c("red","grey60","grey40","grey20"))+
  
  labs(x = "time (s)",
       y = "RR (ms)",
       color = "")+
  theme_bw(base_size = 16)+
  theme(legend.position = c(0.2,1.05),
        legend.justification = c(0,1),
        legend.background = element_blank())

p1


ggsave(filename = paste0(article,"fig2.jpg"),p1,width = 6,height = 3.7)
ggsave(filename = paste0(article,"fig2.tiff"),p1,width = 6,height = 3.7,dpi = 300,
       compression = "lzw")
ggsave(filename = paste0(article,"fig2.eps"),device = "eps",p1,width = 6,height = 3.7,dpi = 300)


###### figure 3 ##########


plot_SDR = function(yvar,tag,xtitle){
  ggplot()+
    geom_point(data = doremi_var,
               aes(get(yvar),SDRR,
                   color = test
                   # shape = test,
               ),
               size = 3,
               stroke = 1.5,
               shape = 21,
               alpha = .6)+
    geom_line(data = model_plot,
              aes(get(yvar),
                  get(paste0("model_",yvar)),
                  linetype = "model1"),
              color = "red",
              size = 1,
              alpha = .6)+
    geom_line(data = model_plot,
              aes(get(yvar),
                  get(paste0("model_",yvar,"_effonly")),
                  linetype = "model2"),
              color = "red",
              size = 1,
              alpha = .6)+
    theme_bw(base_size = 14)+
    theme(plot.title = element_text(hjust = 0.5,size = 16),
          plot.tag.position = c(0, 1),
          legend.key.width  =  unit(0.5, "in"))+
    scale_y_continuous(breaks = c(1,3,10,50,100,150),
                       trans = "log",
                       limits = c(1,NA))+
    # scale_shape_manual(values = c(3,0,2))+
    # scale_color_viridis_d()+
    scale_color_viridis_d(begin = .1,end = .9,
                          breaks = c("recovery","rest","test"),
                          labels = c("recovery","pre-effort","effort"))+
    scale_linetype_manual(breaks = c("model1","model2"),
                          values = c("solid","dashed"),
                          labels = c("all points",
                                     "effort only")
    )+
    labs(x = xtitle,
         y = "SDRR (ms)",linetype = "fit on:",
         color = "GET sequence:",
         tag = tag)+
    guides(linetype = guide_legend(override.aes = 
                                     list(alpha = 1,
                                          linetype = c("solid","dashed"))))
}
plot_SDR("HR_mean","A","mean HR (beat/min)")

p2_1 <- plot_SDR("HR_mean","A","mean HR (beat/min)")+
  guides(color = "none",shape = "none",linetype = "none")

p2_2 <- plot_SDR("pw","B","Power (W)")+
  guides(color = "none",shape = "none",linetype = "none")

p2_3 <- plot_SDR("Intensity_mean","C",
                 expression(paste("mean Intensity (%",VO2[max],")")))


library(gridExtra)
library(grid)
# grid.arrange(arrangeGrob(p1,p2,p3,  nrow = 1))


g1 <- ggplotGrob(p2_1)
g2 <- ggplotGrob(p2_2)
g3 <- ggplotGrob(p2_3)

g <- cbind(g1,g2,g3,size = "first")

grid.newpage()
grid.draw(g)


png(filename = paste0(article,"fig3.png"),
    width = 10,height = 4.5,units = "in",res = 600)
grid.newpage()
grid.draw(g)
dev.off()

jpeg(filename = paste0(article,"fig3.jpg"),
     width = 10,height = 4.5,units = "in",res = 600)
grid.newpage()
grid.draw(g)
dev.off()


tiff(filename = paste0(article,"fig3.tiff"),
     width = 10,height = 4.5,units = "in",res = 300,
     compression = "lzw")
grid.newpage()
grid.draw(g)
dev.off()



###### figure 4 ##########
fig4 <- correlation_res_sens[variable2 == "tau" & 
                               variable1 == "VO2max_abs" & coeff == "tau" &
                               order == 0 & detrend_win == var_winsize] %>% 
  ggplot(aes(detrend_win,-Corr,color = p < 0.05))+
  geom_point(size = 2)+
  scale_color_manual(breaks = c(T,F),
                     values = c("lightblue","orange"),
                     labels = c("p < 0.05","p >= 0.05"))+
  scale_y_continuous(breaks = -seq(0,1,0.2))+
  theme_bw(base_size = 12)+
  theme(legend.position = c(1,1),
        legend.justification = c(1,1),
        legend.background = element_blank(),
        legend.key = element_blank(),
        axis.title.y = element_text(margin = margin(r = 3,unit = "mm")),
        axis.title.x = element_text(margin = margin(t = 3,unit = "mm")))+
  labs(x = expression(paste("windows size ",omega)),
       y = "Pearson correlation between\nHRV decay constant and VO2max",
       # title = "Correlation between HRV decay\n and VO2max",
       color = "")

fig4
tiff(filename = paste0(article,"fig4.tiff"),
     width = 5,height = 4,units = "in",res = 300,
     compression = "lzw")
fig4
dev.off()

#### supp fig -----

library(plyr)
correlation_res_sens[,variable1_r := factor(mapvalues(variable1,
                                                      c("P_sv1","P_sv2","powermax","VO2max_abs"),
                                                      c("PVT1","PVT2","MAP","VO2[max]")),
                                            levels = c("VO2[max]","MAP","PVT1","PVT2"))]
correlation_res_sens[,detrend_method_r := mapvalues(order,c(0:2),
                                                    c("p == 0","p == 1","p == 2"))]

suppS1 <- correlation_res_sens[variable2 == "tau"  & coeff == "tau" &
                                 variable1_r %in% c("PVT1","PVT2","MAP","VO2[max]") &
                                 detrend_win == var_winsize] %>% 
  ggplot(aes(detrend_win,-Corr,color = p < 0.05))+
  geom_point(size = 2)+
  scale_color_manual(breaks = c(T,F),
                     values = c("lightblue","orange"),
                     labels = c("p < 0.05","p >= 0.05"))+
  scale_y_continuous(breaks = -seq(-.2,1,0.2))+
  theme_bw(base_size = 12)+
  theme(axis.title.y = element_text(margin = margin(r = 3,unit = "mm")),
        axis.title.x = element_text(margin = margin(t = 3,unit = "mm")),
        legend.position = "top")+
  labs(x = expression(paste("windows size ",omega)),
       y = "Pearson correlation between\nHRV decay constant and VO2max",
       # title = "Correlation between HRV decay\n and VO2max",
       color = "")+
  facet_grid(variable1_r ~ detrend_method_r ,labeller = label_parsed)


png(filename = paste0(article,"S1_Fig.png"),
    width = 7,height = 8,units = "in",res = 300)
# grid.arrange(grobs = list(p1, p2), ncol=2)
suppS1
dev.off()


jpeg(filename = paste0(article,"S1_Fig.jpg"),
     width = 7,height = 8,units = "in",res = 300)
# grid.arrange(grobs = list(p1, p2), ncol=2)
suppS1
dev.off()

tiff(filename = paste0(article,"S1_Fig.tiff"),
     width = 7,height = 8,units = "in",res = 300)
# grid.arrange(grobs = list(p1, p2), ncol=2)
suppS1
dev.off()



######### tables -----

#### table 1 ####
basic_data[,VO2max := VO2max*1000]
setnames(basic_data,
         c("age",
           "powermax","VO2max","HRmax",
           "P_sv1","P_sv2","HRR60","sport"),
         c("Age",
           "Maximum Power","VO2 max",
           "HR max","Power VT1","Power VT2",
           "HRR","Sport"))


table1 <- CreateTableOne(vars = c("Age","sex",
                                  "Weight","Height",
                                  "Maximum Power","VO2 max",
                                  "HR max","Power VT1",
                                  "Power VT2","HRR","Sport"),
                         factorVars = c("sex","Sport"), 
                         data = basic_data)

table1_flex <- print(table1,nonnormal = T,contDigits = 1) %>% 
  as.data.table(keep.rownames = T)%>%
  flextable()%>%
  autofit()

# table 2
get_tau = function(sumary){
  sumary %>%
    broom::tidy() %>%
    filter(term == "a") %>% 
    pull(estimate) %>% 
    (function(x) -1*log(2)/x)
}

model_table <- rbind(sapply(model_res,AIC),sapply(model_res,BIC),sapply(model_res,get_tau))%>%
  t()%>%
  as.data.table(.,keep.rownames = T)%>%
  setnames(c("rn","V1","V2","V3"),c("model","AIC","BIC","tau"))
cmodel_table[,AIC := round(AIC)]
model_table[,BIC := round(BIC)]

table2_flex <- flextable(model_table) %>% 
  colformat_num(big.mark="", decimal.mark = ".") %>%
  autofit()

##### table 3 ----


sign = function(p){
  
  fcase(p < 0.001,"***",
        p < 0.01,"**",
        p < 0.05,"*",
        default = "")
}

conv_pvalue = function(x){
  fcase(x < 0.001,"<0.001",
        x < 0.01,as.character(round(x,3)),
        x >= 0.01, as.character(round(x,2)
        )
  )
}

res_doremi[,tau := -log(2)/a]

plouf <- rcorr(as.matrix(res_doremi[,.(tau,b,VO2max_abs,powermax,P_sv1,P_sv2,HRmax,HRR60)]),
               type = "pearson")

table3 <- data.table(var = names(plouf$r[,c("tau")]))
table3[,tau := round(plouf$r[,c("tau")],2)]
table3[,tau_p := conv_pvalue(plouf$P[,c("tau")])]
table3[,b := round(plouf$r[,c("b")],2)]
table3[,b_p := conv_pvalue(plouf$P[,c("b")])]

table3_flex <- flextable(table3) %>% autofit()

# keep table in docx

doc <- read_docx()%>%
  body_add_flextable(table1_flex)%>%
  body_add_break()%>%
  body_add_flextable(table2_flex)%>%
  body_add_break()%>%
  body_add_flextable(table3_flex)%>%
  body_add_break()%>%
  print(doc,target = paste0(article,"tables.docx"))
