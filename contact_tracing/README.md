This is the repository containing the data and code for the article "Coverage of state-initiated contact-tracing during COVID-19 and factors influencing it: evidence from real-world data" by Denis Mongin, Nils Bürgisser, Delphine Sophie Courvoisier, and the Covid-SMC Study Group.

The repository is organised as follow:

- the folder [vaccination_functions](./vaccination_functions) contains the scripts allowing to determine the vaccination status from the date of vaccination and the type of vaccine.
- The file  anonym_data.RData contains a sample of anonymised data:
   - `df` is the table with the infections occurring at a given address, with one line per infection: 
       - Addresses are in the `idpadr` variable
       - `building_cat` is the type of building
       - `person_id` id the identifier of the person infected
       - `infection_id` the unique identifier of the infection
       - `date_res` the date of the infection
       - `SSECTEUR_7` is the identification of the neighborhood corresponding to the address
   - `adresse_loc` contains additional information for the addresss, such as the CATI index
   - `person_immune_long` contains the date of vaccination `date` and type of vaccine of the persons
   - `table_contact` is the table of declared contacts: the person `person_id_index`, with a positive result at `date_res_index` declared the person `person_id_contact` as his/her contact. The contact may have had a positive test at date `date_res_contact`. The variable `contact_id` is the unique identifier of the index-contact relation for a given infection.
   - `table_infection` provides the list of infection dates, for the figure 2
   - `table_person` provides the birth year and the gender `genre` (2: women, 1: men) of the person identified by `person_id`
- The file [main_study.R](./main_study.R) contain the script for the main study. The function `generate_combination` generate the permutations. The different sections correspond to the different part of the analysis:
   - delay study is concerned with the calculation of secondary infection as a function of the delay between the tests
   - Number of secondary infection, per group is concerned with the calculation of secondary infection stratified per variant, or in function of time (i.e. per months)
   - multivariable analysis is concerned with the glm analysis
- The file [figure_tables.R](./figure_tables.R) produce the figures and table of the manuscript from the output of the `main_study.R` script.

   

