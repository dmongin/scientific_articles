# HRR_comparison

The code is the code associated with the article
"Time to publication and time-lag publication bias for randomized trials on connective tissue diseases.", by Denis MONGIN, Barbara RUSSO, Alejandro BRIGANTE, Sami CAPDEROU, Delphine S. COURVOISIER, Michele IUDICI

It aims at assessing the time from completion to publication of randomized controlled trials (RCTs) on connective tissue diseases (CTDs), and investigating the factors associated with, and explore the influence of significance of study results on time to publication (time-lag publication bias).

The file [Analysis](./Analysis.R) perform the analysis, create the tables and figures.

The file [published_RCT_clean](./published_RCT_clean.csv) contains the data concerning the published RCT, with the following variables:


