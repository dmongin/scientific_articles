# Reporting of and representativeness of race, ethnicity and socioeconomic status in systemic sclerosis randomized trials: an observational study.

This folder contains the data and the code used for the publication "Reporting of and representativeness of race, ethnicity and socioeconomic status in systemic sclerosis randomized trials: an observational study.", by Edouard CHAIX, Denis MONGIN, Cem GABAY, and  Michele IUDICI.

- The file SSc_RCT_data.xlsx contains the data about the 106 SSc-RCTs included in the study.
- The file IF_complete.csv contains the Impact factor of the journals.
- The folder [census](./census) contains the US census data per race from 2000 to 2020, the R code [load_census.R](./census/load_census.R) allowing to merge these data into one table, and the output table saved in a RData file called census_prop.RData.
- The R file [analysis.R](analysis.R) contains the analysis code, creating the tables and figures of the article

