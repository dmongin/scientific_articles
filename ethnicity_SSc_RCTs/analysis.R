library(data.table)
library(lubridate) 
library(magrittr) 
library(ggplot2) 
library(flextable)
library(readxl)
library(stringr)
library(officer)
library(dplyr)
library(tableone)
library(patchwork)


#  Load #####

rm(list = ls())

setwd(dirname(rstudioapi::getActiveDocumentContext()$path))

data_raw <- readxl::read_xlsx("SSc_RCT_data.xlsx") %>% setDT()

load("./data_census/census_prop.RData")


data = data_raw[,.(ID = `ID paper`,year = as.numeric(as.character(`Publication Year`)),
                   Continent,
                   country = `Country(es)`,
                   Funding,Intervention,
                   journal = tolower(`Journal/Impact factor`) %>% trimws(),
                   SSc = `SSc subtype according to Leroy`,
                   organ = `SSc complication investigated`,
                   age = Age,Npatients = `N of patients randomized`,
                   ethnicity_report = `Race/Ethnicity reported`,
                   White = `White or Caucasian N`,Black = `Black or African American N`,
                   AIAN = `American Indian or Alaskan Native N`,
                   Asian = `Asian N`,
                   NHPI = `Native Hawaian or Pacific Islander N`,
                   Hispanic = `Hispanic or Latino N`,
                   North_african = `North African N`,
                   Middle_east = `Middle Eastern N`,
                   Others = `Other N`,
                   adjustement = `Use of race/ethnicity as adjustment factor for statistical models`
)]

# local DM #####

data[,c("SSc","Funding","Intervention") := lapply(.SD,function(x){
  as.factor(tolower(x))
}),.SDcols = c("SSc","Funding","Intervention")]

data[,Funding := relevel(Funding,"non-industry")]
data[,year := as.numeric(as.character(year))]

data[,year_cat := factor(
  fifelse(year >= 2010,"2010-2020","2000-2010"),
  levels = c("2000-2010","2010-2020")
)]

data[,Npatient_cat := cut(Npatients,c(-1,50,100,1e6),c("<50","[50-100[",">=100"),right = F)]

data[,Continent_r := fcase(Continent == 0,"Europe",
                           Continent == 1,"north America",
                           Continent == 2,"South America",
                           Continent == 3,"Asia",
                           Continent == 4,"Africa",
                           Continent == 5 | Continent == 6,"Intercontinental")]

data[,lung := fifelse(organ == "Lung" | 
                        organ == "Pulmonary Hypertension" | 
                        organ == "Skin AND Lung",1,0)]

data[,skin := fifelse(organ == "Skin" | 
                        organ == "Skin AND gastrointestinal" | 
                        organ == "Skin AND Lung",1,0)]

data[,gastro := fifelse(organ == "Gastrointestinal" | 
                          organ == "Skin AND gastrointestinal",1,0)]

data[,Raynaud := fifelse(organ == "Raynaud's phenomenon",1,0)]
data[,no_complication := fifelse(organ == "No specific complication",1,0)]
data[,Others_old := Others]
data[,Others := rowSums(.SD),.SDcols = c("North_african","Middle_east","Others_old")]

data[,Funding := factor(Funding,levels = c("industry","non-industry","jointly funded","not stated"))]
data[,Continent_r := factor(Continent_r,
                            levels = c("Europe","north America","South America","Asia","Africa","Intercontinental"))]
data[,SSc := factor(SSc,levels = c("dcSSc","lcSSc","both","not specified"))]
# functions ###3

pvalu_format = function(val){
  out <- ifelse(val >= 0.01,formatC(val,digit = 2,format = "f"),
                ifelse(val %between% c(0.001,0.01),formatC(val,digit = 3,format = "f"),
                       ifelse(val < 0.001,"<0.001","   ")
                )
  )
  out <- ifelse(out == 0.05,formatC(val,digit = 3,format = "f"),out)
  
}

sign_p = function(val){
  ifelse(val %between% c(0.01,0.05),"*  ",
         ifelse(val %between% c(0.001,0.01),"** ",
                ifelse(val < 0.001,"***","   ")
         )
  )
}

format_digit = function(vec){
  out <- sapply(vec,function(x) format(x,digits = 1,nsmall = 1,scientific = F))
  fifelse(as.numeric(out) == 1,formatC(vec,digits = 3,format = "g"),out)
}


# tables ####
## table 1 ###########


table1 <- CreateTableOne(data = data,
                         vars = c("year_cat","Funding","Continent_r",
                                  "SSc","lung","skin","gastro",
                                  "Raynaud","no_complication","Npatients",
                                  "Npatient_cat","Intervention"),
                         factorVars = c("year_cat","Funding","Continent_r",
                                        "SSc","lung","skin","gastro",
                                        "Raynaud","no_complication",
                                        "Npatient_cat","Intervention"), 
                         strata = "ethnicity_report",addOverall = T
) %>% print(nonnormal = "Npatients",contDigits = 0,catDigits = 0,
            pDigits = 2,
            cramVars  = c("year_cat"),noSpaces = T) %>% 
  as.data.table(keep.rownames = T)

# add % in the parenthesis
table1[,Overall := str_replace(Overall,"(?<=\\()([0-9]+)(?=\\))","\\1%")]
table1[,`0` := str_replace(`0`,"(?<=\\()([0-9]+)(?=\\))","\\1%")]
table1[,`1` := str_replace(`1`,"(?<=\\()([0-9]+)(?=\\))","\\1%")]
# subselect column of interest
table1 <- table1[,.(rn,Overall,`1`,`0`,p)]


## table 2 #####

# sum of ethnics sount
data[,sumethnic := rowSums(.SD),
     .SDcols = c("White","Black","AIAN","Asian","NHPI","Hispanic","Others")]

# calculate proportion
ethnic_vars <-  c("White","Black","AIAN","Asian","NHPI","Hispanic","Others")
data[,c(paste0(ethnic_vars,"_prop")) := lapply(.SD,function(x) round(x/sumethnic*100,2)),.SDcols = ethnic_vars]
# binary US variable
data[,US := fifelse(country == "USA",1,0)]

# tab le per periode
table2 <- CreateTableOne(data = data[ethnicity_report == 1],
                         strata = "year_cat",
                         vars = paste0(ethnic_vars,"_prop"),
                         addOverall = T) %>%
  print(nonnormal = T,contDigits = 0) %>% 
  as.data.table(keep.rownames = T)

# add % in the parenthesis
table2[,c("Overall","2000-2010","2010-2020") := 
         lapply(.SD,function(x) str_replace_all(x,"(\\d+)","\\1%")),
       .SDcols = c("Overall","2000-2010","2010-2020")]

# alternative table per US or not
table2bis <- CreateTableOne(data = data[ethnicity_report == 1],
                            strata = "US",
                            vars = paste0(ethnic_vars,"_prop"),
                            addOverall = T) %>%
  print(nonnormal = T,contDigits = 0) %>% as.data.table(keep.rownames = T)



## table3: regression #####

# load IF data
# obtained from clarivate
IF <- fread("IF_complete.csv")

# merge them
data[IF,IF := i.V3,on = "journal"]

# simplified continent variable
data[,Continent_r2 := Continent_r]
data[Continent_r2 %in% c("South America","Africa","Asia"),Continent_r2 := "other"]

# glm model
fit <- glm(ethnicity_report ~ year +  IF + Continent_r2,
           data = data[year < 2021],family = "binomial") 

summary(fit)

# check colinearities
library(performance)
check_model(fit)

# extract output
tmp <- broom::tidy(fit) %>% 
  setDT()
table3 <- tmp[term != "(Intercept)",
              .(term,
                HR = paste0(format_digit(exp(estimate)),
                            sign(p.value)," [",
                            format_digit(exp(estimate - 1.96*std.error)),
                            ",",
                            format_digit(exp(estimate + 1.96*std.error)),
                            "]"),
                p.value = pvalu_format(p.value))]


## table4: global numbers #######

# load census data
load("./census/census_prop.RData")

# prevalence: table 2 from 10.1002/art.11073
prevalence = data.table(race = c("White","Black"),
                        prev = c(224/1e6,315/1e6))


global_white_black <- lapply(list(2000:2009,2010:2020,2000:2020), # loop per period
                             function(period){
  
  # population of white and black from american census
  census <- census_tot[race %in% c("White","Black") & year %in% period,
                          .(pop = mean(pop)),by = race]
  # expected prevalence of SSc
  census[prevalence,NSSc := round(pop*i.prev),on = "race"]
  
  # number of patient in SSc-RCTs
  SSC_rct = data.table(race = c("White","Black"),
                       Npatient = c(sum(data[year %in% period & country == "USA",White],na.rm = T),
                                    sum(data[year %in% period & country == "USA",Black],na.rm = T)))
  # merge
  tot <- merge(SSC_rct,census,by = "race")
  # chi squarre test
  test <- chisq.test(cbind(census$NSSc,SSC_rct$Npatient))
  # add info
  tot[,years := paste0(period[1],"-",last(period))]
  tot[,p := test$p.value]
  
}) %>% rbindlist()

# format table
table4 <- melt(global_white_black,measure.vars = c("pop","NSSc","Npatient")) %>%
  dcast(formula = years + p  ~ paste0(variable,"_",race) )

# calculate ratios
table4[,census_ratio := round(NSSc_White/NSSc_Black,1)]
table4[,patient_ratio := round(Npatient_White/Npatient_Black,1)]

# reorder columns
setcolorder(x = table4,c("years","pop_White","pop_Black","NSSc_White","NSSc_Black","Npatient_White",
                         "Npatient_Black","census_ratio","patient_ratio","p"))

# format p value
table4[,p := pvalu_format(p)]


# plots #######

## long format #####
data_long <- melt(data,
                  measure.vars = c("White","Black","AIAN","Asian","NHPI","Hispanic","Others"),
                  value.name = "N_ethnic",
                  variable = "ethnic")

data_long[,ethnic := factor(ethnic,
                            levels = c("Hispanic",
                                       "AIAN","Asian","Black",
                                       "NHPI","Others","White"))]
data_long[ethnicity_report == 1,sumethnic := sum(N_ethnic),
          by = ID]

data_long[ethnicity_report == 1,Nethnic_year := sum(N_ethnic),by = year]

# proportions per year
plot <- data_long[ethnicity_report == 1,
                  .(prop_etnic = round(sum(N_ethnic)/Nethnic_year[1]*100,2)),
                  by = .(ethnic,year)]

plot[ethnic %in% c("North_african","Middle_east"),ethnic := "Others"]
plot[,ethnic := factor(ethnic,
                       levels = c("Hispanic",
                                  "AIAN","Asian","Black",
                                  "NHPI","Others","White"))]


## plot #######

p1 <- data %>%
  filter(year < 2021) %>%
  ggplot()+
  geom_bar(aes(year,
               fill = as.factor(ethnicity_report)),
           alpha = .7)+
  
  scale_fill_viridis_d(breaks = c(0,1),
                       labels = c("Race/ethnicity not reported",
                                  "Race/ethnicity reported"))+
  labs(fill = "",y = "N of SSc-RCTs",x = "")+
  scale_x_continuous(breaks = 2000+0:4*5,limits = c(1999,2021))+
  scale_y_continuous(breaks = 0:8*2)+
  theme_minimal()+
  theme(legend.position = c(0.1,.9),
        legend.justification = c(0,1),legend.background = element_blank())

p1


# define race color
coul <- brewer.pal(length(unique(plot$ethnic)),"Accent") 

df_colors <- data.table(
  enthnic = unique(plot$ethnic),
  coul = coul
)


p2 <- ggplot(plot,aes(year,prop_etnic,fill = ethnic))+
  geom_col(alpha = .8)+
  theme_minimal()+
  scale_fill_manual(breaks = df_colors$enthnic,
                    values = df_colors$coul)+
  scale_x_continuous(breaks = 2000+0:4*5,limits = c(1999,2021))+
  theme(legend.position = "bottom",
        panel.grid.minor.y = element_blank())+
  guides(fill = guide_legend(nrow = 1))+
  scale_y_continuous(
    breaks = c(0,25,50,75,100),labels = paste0(c(0,25,50,75,100),"%")
  )+
  labs(x = "",y = "Proportion of racial/ethnic\ngroups in SSc-RCTs",fill = "")
# p2

p_tot <- p1/p2 + 
  plot_layout(heights  = c(2, 1)) + 
  plot_annotation(tag_levels = "A")



# save #########

# tables
read_docx() %>%
  body_add_flextable(flextable(table1) %>% autofit())%>%
  body_add_break() %>%
  body_add_flextable(flextable(table2) %>% autofit())%>%
  body_add_break() %>%
  body_add_flextable(flextable(table3) %>% autofit())%>%
  body_add_break() %>%
  body_add_flextable(flextable(table4) %>% autofit())%>%
  body_add_break() %>%
  print(target = "./figures_tables/tables.docx")

# figure
tiff("./figures_tables/figure1.tiff",
     units="in", height = 7,width = 7, res=300, compression = "lzw")
p_tot
dev.off()