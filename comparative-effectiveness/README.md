# Data from: Accounting for missing data caused by drug cessation in observational comparative effectiveness research: a simulation study

This repository provides the code and the simulated dataset used for the simulation study "Accounting for missing data caused by drug cessation in observational comparative effectiveness research: a simulation study", by Denis Mongin, Kim Lauper, Thomas Frizel, Axel Finckh, and Delphine Courvoisier (http:// dx. doi.org/ 10. 1136/ annrheumdis-2021- 221477).

The files are:

- [**parameter.csv**](parameter.csv) contains the different simulation conditions used
- [**generate_simu_data.R**](generate_simu_data.R), provides the code used to create the simulation datasets from the true register collection. One dataset is generated per simulation condition, with an associated probability of missing calculated from the glm model performed on the initial collection of registry (imputed with mice). The  13 resulting datasets can be found in the [data_for_simu](./data_for_simu/) folder. 
- [**functions.R**](functions.R) contains the different methods used to estimate the comparative effectiveness. Each function takes as input a dataframe containing one line per patient-treatment, the variable name used to define effectiveness (measured at 12 month), the variable name indicating the treatments, the treatment reference value, the potential confounders, and the variables used to model attrition.
- [**simulation.R**](simulation.R) contains the simulation script. The results for each parameter condition are stored in the folder [simu_results](./simu_results/).
- [**analysis.R**](analysis.R) merges the different result files and produces the figures and tables.


The file [**example.Rmd**](example.Rmd) provides an example of the imputation functions on one simulation dataset.
