---
title: "data management"
output: html_notebook
---

Here is the data management to calculate the different test characteristics and HRR indices

```{r}
library(data.table) # data handling
library(ggplot2) # for the plots
library(zoo) # for rolling mean

rm(list= ls()) # clean space
setwd(dirname(rstudioapi::getActiveDocumentContext()$path)) # set active directory

if(!file.exists("subject-info.csv"))
{
  download.file("https://physionet.org/files/treadmill-exercise-cardioresp/1.0.0/subject-info.csv",
                destfile = "subject-info.csv")
}

if(!file.exists("test_measure.csv"))
{
  download.file("https://physionet.org/files/treadmill-exercise-cardioresp/1.0.0/test_measure.csv",
                destfile = "test_measure.csv")
}

ID_info <- fread("subject-info.csv")
test_measure <- fread("test_measure.csv")
```

# characterization of the effort test zones

We find the warming, effort and recovery periods of the effort tests. Let us first define an index of each speed change
```{r}
test_measure[,Speed_idx := rleid(Speed),by = ID_test]
test_measure[,Speed_idx_max := max(Speed_idx),by = ID_test]
```

We then calculate each change of speed, and define recovery has the decrease of speed period, warming as the beginning preceeding the effort (which can include an increase of speed from 0 to 5km/h, but longer than the speed steps during the effort), and effort the period of speed increase

```{r}
speed_steps <- test_measure[,.(Speed = Speed[1],
                               dt_step = time[.N]-time[1]),by = .(ID_test,Speed_idx)]
speed_steps[,Speed_idx_max := max(Speed_idx),by = ID_test]
speed_steps[,speed_diff := c(0,diff(Speed)),by = ID_test]


speed_steps[speed_diff > 0 & dt_step <= 75,stage := "effort"]
speed_steps[(speed_diff == 0 | (speed_diff >= 0 & dt_step > 75) | Speed < 4.9) & Speed_idx <= 5,stage := "warming"]
speed_steps[,recov := rleid(speed_diff < 0 & Speed_idx >= (Speed_idx_max - 4)) - 1,by = ID_test]
speed_steps[recov >= 1,stage := "recovery"]
test_measure[speed_steps,stage := i.stage,on = c("ID_test","Speed_idx")]

```

Plot to observe the effort zones:

```{r,fig.height=20,fig.width=8}
set.seed(123)
tirage <- sample(unique(ID_info[,ID_test]),21)

ggplot(test_measure[ID_test %in% tirage])+
  geom_line(aes(time,Speed*10,color = "speed"))+
  geom_line(aes(time,HR,color = "HR"))+
  geom_ribbon(aes(time,ymin = 0,ymax = HR,fill = stage),alpha = 0.3)+
  theme_light()+
  facet_wrap(~ID_test,scale = "free",ncol = 3)
```

The difference between ramping and incremental test can be seens in the speed increment during the effort. If speed increases are smaller than 10s, then it is a ramping protocol

```{r}
setkey(test_measure,time)
step_durations <- speed_steps[ ,.(mean_dt_step = mean(dt_step[stage == "effort"],na.rm = T)),by = ID_test]
effort_duration <- test_measure[stage == "effort",.(effort_duration = last(time ) - first(time)),by = ID_test]
recov_duration <- test_measure[stage == "recovery",.(recovery_duration = last(time ) - first(time)),by = ID_test]
pentes <- test_measure[stage == "effort",.(slope = round((Speed[1]-last(Speed))/(time[1] - last(time))*60,2)),by = ID_test] 

effort_info <- Reduce(function(x,y) merge(x,y,by = "ID_test"),
                      list(step_durations,effort_duration,recov_duration,pentes))
effort_info[,protocol_type := as.factor(fifelse(mean_dt_step < 10,"ramping","incremental"))]
effort_info %>% summary()
```

Calculation of VO2max, HRpeak, and MAS 

```{r}
max2 = function(x){
  if(length(x)>0){max(x)}else{as.numeric(NA)}
}
min2 = function(x){
  if(length(x)>0){min(x)}else{as.numeric(NA)}
}
result_physio <- test_measure[,.(HRpeak = max2(rollmean(na.omit(HR),10)),
                                 VO2max = max2(rollmean(na.omit(VO2),10)),
                                 Speedmax = max(Speed,na.rm = T),
                                 VCO2max = max2(rollmean(na.omit(VCO2),10))),
                              by = ID_test]
```

# Calculate HRR indices

```{r}
test_measure[result_physio,HRpeak := i.HRpeak,on = "ID_test"]
test_measure[stage == "recovery", time_rest := time - time[1], by = ID_test] # define time since recovery start
HRR_results <- test_measure[stage == "recovery" & !is.na(HR), 
                              .(HRR60 = HRpeak[1]-HR[time_rest>=60][1],
                                HRR30 = HRpeak[1]-HR[time_rest>=30][1],
                                HRR10 = HRpeak[1]-HR[time_rest>=10][1],
                                HRR120 = HRpeak[1]-HR[time_rest>=120][1],
                                HRR180 = HRpeak[1]-HR[time_rest>=180][1],
                                HRRat60 = HR[time_rest>=60][1],
                                HRRat30 = HR[time_rest>=30][1],
                                HRRat10 = HR[time_rest>=10][1],
                                HRRat120 = HR[time_rest>=120][1],
                                HRRat180 = HR[time_rest>=180][1],
                                HRRpc60 = HR[time_rest>=60][1]/HRpeak[1]*100,
                                HRRpc30 = HR[time_rest>=30][1]/HRpeak[1]*100,
                                HRRpc10 = HR[time_rest>=10][1]/HRpeak[1]*100,
                                HRRpc120 = HR[time_rest>=120][1]/HRpeak[1]*100,
                                HRRpc180 = HR[time_rest>=180][1]/HRpeak[1]*100
                              ),
                              by = ID_test]
```

T30 HRR indice

```{r}
test_measure[,lnHR := log(HR)]
T30 <- test_measure[time_rest %between% c(10,40),.(HRRT30 = {
  plof <- lm("lnHR~time_rest",data = .SD) %>% summary
   plof$coefficients["time_rest","Estimate"]*1000
  })
  ,by = ID_test]
```

calculation of exponential regression indices

```{r}
idlist <- unique(test_measure$ID_test)
library(progress)
pb <- progress_bar$new(total = length(idlist))
exp_fit_long <- rbindlist(lapply(seq(idlist),function(i){

  iddate <- idlist[i]
  pb$tick()
  
  fitexprun <- lapply(c(30,60,120,180),function(to){
    signal <- test_measure[stage == "recovery" & ID_test == iddate  & time_rest <= to]
    plouf <- tryCatch({
      fitexp <- nls(HR ~ a*exp(-time_rest/tau) + b,
                    data = signal,
                    # start = data.frame(a = c(50,350), tau = c(50,200), b = c(50,400)),
                    start = list(a = 100, tau = 100, b = 80),
                    lower = c(0,0,-100),
                    upper = c(300,1000,150),
                    algorithm = "port")
    },error = function(e){e})
    if(!inherits(plouf,"error")){
      signal[,HR_fit1exp := predict(plouf,newdata = signal)]
      tau <-  summary(plouf)$coefficients["tau","Estimate"]
      a  <-  summary(plouf)$coefficients["a","Estimate"]
      b <-  summary(plouf)$coefficients["b","Estimate"]
    }else{
      signal[,HR_fit1exp := NA]
      tau <- NA
      a  <- NA
      b <- NA
    }
    R2_1 <- signal[,1- sum((HR - HR_fit1exp)^2)/sum((HR - mean(HR,na.rm = T))^2)]
    data.table(R2 = R2_1,tau = tau,tautime = to,ID_test = iddate,a = a,b = b)
  }) %>% rbindlist()
  fitexprun

}))
exp_fit_long[R2 < 0.8,tau := NA]
exp_fit_long[R2 < 0.8,R2 := NA]

exp_fit <- dcast(exp_fit_long, ID_test~ paste0("HRRtau",tautime),value.var = "tau")
exp_fit_b <- dcast(exp_fit_long, ID_test~ paste0("HRRtau",tautime),value.var = "b")
exp_fit_R2 <- dcast(exp_fit_long, ID_test~ paste0("HRRtau",tautime),value.var = "R2")

```

merging all results

```{r}
indices_results <- Reduce(function(x,y) merge(y,x,by = "ID_test"),
                           list(ID_info,HRR_results,exp_fit,T30,result_physio,effort_info))
indices_results[,VO2maxkg := VO2max/Weight]
head(indices_results)
```

```{r}
lm(HRpeak~Age,data = indices_results) %>% summary()
```

```{r}
ggplot(indices_results,aes(Age,HRpeak,fill = as.factor(Sex)))+
  geom_point(shape = 21)
```


```{r}
save(indices_results,file = "indices.RData")
```

