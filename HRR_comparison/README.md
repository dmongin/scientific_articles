# HRR_comparison

The code is the code associated with the publication https://doi.org/10.1080/15438627.2021.1954513: 
"Heart rate recovery to assess fitness: comparison of different calculation methods in a large cross-sectional study", by Denis Mongin, Clovis Chabert, Delphine Sophie Courvoisier, Jeronimo
Garc�a-Romero & Jose Ramon Alvero-Cruz.

It aims at comparing Heart rate recovery calculation, using the data published in pyysionet: 
Mongin, D., Garc�a Romero, J., & Alvero Cruz, J. R. (2021). Treadmill Maximal Exercise Tests from the Exercise Physiology and Human Performance Lab of the University of Malaga (version 1.0.1). PhysioNet. https://doi.org/10.13026/7ezk-j442.

The file [data_management](./data_management.Rmd) download the data, create some plots, and calculate the different HRR.

The file [analysis.Rmd](./analysis.Rmd) perform the analysis and create the tables published in the article.
