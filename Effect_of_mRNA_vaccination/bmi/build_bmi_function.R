library(pdftools)
library(data.table)
library(lubridate) # for dates
library(stringr)

rm(list = ls())
setwd(dirname(rstudioapi::getActiveDocumentContext()$path))

plouf <- pdf_text("New_cut_off_points_female_children.pdf")
plouf <- str_replace_all(plouf,"Age months","age_m")
plouf <- str_replace_all(plouf,"Age \\(years\\)","age_y")

bmi_girls <- lapply(plouf,fread) %>% rbindlist

plouf <- pdf_text("New_Cut_off_Points_Male_Children.pdf")
plouf <- str_replace_all(plouf,"Age months","age_m")
plouf <- str_replace_all(plouf,"Age \\(years\\)","age_y")

bmi_boys <- lapply(plouf,fread) %>% rbindlist


obese_f = loess(bmi ~ Age,data = bmi_girls[,.(Age = age_y,bmi = `30`)],span = .15,control=loess.control(surface="direct"))
obese_h = loess(bmi ~ Age,data = bmi_boys[,.(Age = age_y,bmi = `30`)],span = .15,control=loess.control(surface="direct"))

test <- rbind(bmi_boys[,.(Age = age_y,bmi = `30`)],
              data.table(Age = seq(0,2,.1)),fill = T)
test[,predict := predict(obese_h,newdata = .SD)]

library(ggplot2)

test %>%
  ggplot()+
  geom_point(aes(Age,bmi),shape = 21,size = 2)+
  geom_line(aes(Age,predict),color = "red")


# create the function giving status
bmi_status = function(age,
                      sex,
                      bmi){
  # upper and lower limit of bmi
  uph <- predict(obese_h,newdata = data.frame(Age = age))
  upf <- predict(obese_f,newdata = data.frame(Age = age))
  
  # harmonize for sex
  up <- fifelse(sex == 1,predict(obese_h,newdata = data.frame(Age = age)),
                predict(obese_f,newdata = data.frame(Age = age)))
  
  fifelse(age <= 18, # use data for kids and teenagers
          fifelse(bmi > up,"Obesity",
                  "Normal"),
          # use constant cases
          fifelse(bmi > 30,"Obesity",
                  "Normal")
  )
  
}


save(bmi_status,obese_h,obese_f,file = "bmi_stat_func.RData")


