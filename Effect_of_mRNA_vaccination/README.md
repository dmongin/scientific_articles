# Effect of mRNA vaccination and previous infections on SARS-CoV-2 transmission across four variants: adjusted analysis of 111’432 declared contacts  

This is the R code associated with the analysis for the article "Effect of mRNA vaccination and previous infections on SARS-CoV-2 transmission across four variants: adjusted analysis of 111’432 declared contacts", by Denis Mongin, Nils Bürgisser, Gustavo Laurie, Guillaume Schilmmel, Diem-Lan Vu, Stephane Cullati, Delphine Sophie Courvoisier, and the Covid-SMC Study Group.

The code is organized as follows:

- the file [clean_analysis.R](clean_analysis.R) contains the analysis, including the multiple imputation, and the estimations of the SAR using GEE.
- the file [figures.R](figures.R) contains the the code to produce the figures and table, based on the results of the [clean_analysis.R](clean_analysis.R) file.
- the folder [./variant](./variant) contains the data and the code to fit the variant predominance periods. Data stem from https://github.com/hodcroftlab/covariants/blob/master/cluster_tables/SwissClusters_data.json and correspond to the data displayed in https://covariants.org/per-country?region=Switzerland, which are data aggregated from wasterwater analysis and published by Global Initiative on Sharing All Influenza Data (GISAID, https://gisaid.org/) .
- the folder [bmi](./bmi) contains the data and teh code produce the bmi_stat_func.RData, containing the bmi_status function, allowing to identify obese persons, based on age, sex and bmi. Data stem from the publication "Cole TJ, Lobstein T. Extended international (IOTF) body mass index cut-offs for thinness, overweight and obesity. Pediatric Obesity. 2012;7(4):284–94."

The data is composed of 3 datasets. 
Example datasets are provided, which should allow to run the code, but are not based on real data and **will not allow to reproduce the study results**. 

Raw data can be queried using the following form https://edc.hcuge.ch/surveys/?s=TLT9EHE93C . 

- table_contact_example.csv contains data associated with the index/contact relation, having the following variables:
   - year_lastcontact	: year of teh last contact between the index case and its contact
   - person_id_contact : identifier of the contact
   - person_id_index	: identifier of the index
   - infection_id_index	: identifier of the infection
   - contact_type	: type of contact between contact and index
   - N90_cat	: number of COVID-19 tests performed by the contact the last 3 months before the encounter with the index (0, 1, 2+)
   - N180_cat	: number of COVID-19 tests performed by the contact the last 6 months before the encounter with the index (0, 1, 2, 3+)
   - contact_pos	: did the contact became positive during the 10 days after its last contact (1: yes, 0: no)
   - contact_pos2	: did the contact became positive between 4 and 10 days after its last contact (1: yes, 0: no)
   - same_adress	: did the index and the contact live at the same address (1: yes, 0: no)
   - same_adress_pro	: did the index and the contact work at the same address (1: yes, 0: no)
   - immune_stat_contact	: immune status of contact
   - immune_stat_index	: immune status of index
   - immune_stat_index2	: immune status of index
   - variant : Sars-CoV-2 variant of concern, estimated for predominance period of 50%
   - variant_thres90 : Sars-CoV-2 variant of concern, estimated for predominance period of 90%
   - contact_decision: was the contact placed in quarantaine (1: yes, 0: no)
   - Ncontact: number of contacts of the index
   
- table_persons_example.csv 
   - person_id	: person identifier
   - genre	: gender (1: male, 2: female)
   - bmi	: body mass index
   - birthyear	: year of birth
   - cati_score	: socio-economic status of the living neighborhood
   - str_collective	: type of living buiding
   - vuln_perso : is the person vulnerable (1: yes, 0: no)
   
- table_symptoms_example.csv
   - infection_id : identifier of infection
   - any_symptomes : has any symptom (1: yes, 0: no)
   - toux : has cough (1: yes, 0: no)
   - age	: age in year
   - genre	: gender (1: male, 2: female)
   - bmi	: body mass index
   - variant : Sars-CoV-2 variant of concern


