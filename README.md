# scientific_articles

This page gather the R code used in several publications:

- [Heart rate recovery to assess fitness: comparison of different calculation methods in a large cross-sectional study](./HRR_comparison) 
- [Accounting for missing data caused by drug cessation in observational comparative effectiveness research: a simulation study](./comparative-effectiveness) 
- [Decrease of heart rate variability during exercise: an index of cardiorespiratory fitness](./decrease_HRV_effort)
- [A tutorial on ordinary differential equations in behavioral science: what do physics teach us?](./tutorial_differential_equation)
- [Reporting of and representativeness of race, ethnicity and socioeconomic status in systemic sclerosis randomized trials: an observational study](./ethnicity_SSc_RCTs)
