library(deSolve)

ODE2 <- function(t, x, parms) {with(as.list(c(parms, x)), {
  # import <- excfunc(time)
  excitationin <- c(rep(0,25),rep(1,25),rep(0,50))
  import <- excfunc(t)
  dS2 <- dS1
  # dS1<- -(2*pi/period)*amortfact*dS1  -(2*pi/period)^2*S1 + -(2*pi/period)^2*k*(import + yo)
  dS1 <- -(2*pi/period)*2*amortfact*dS1  -(2*pi/period)^2*S1 + (2*pi/period)^2*k*(import + yeq)
  res <- c(dS2,dS1)
  list(res)})}## 

solution_ODE2 = function(period,amortfact,k,yeq, yo,excitation,time){
  excfunc <- approxfun(time, excitation, rule = 2)
  parms  <- c(period = period,amortfact = amortfact, k = k, yeq = yeq, excfunc = excfunc)
  xstart = c(S1 =  yo,dS1 = 0)
  out <-  lsoda(xstart, time, ODE2, parms)
  return(out[,2])
}


ODE1 <- function(time, x, parms) {with(as.list(c(parms, x)), {
  import <- excfunc(time)
  dS <- import*k/tau - (S-yeq)/tau 
  res <- c(dS)
  list(res)})}

# function de génération d'une solution pour une excitation donnée
solution_ODE1 = function(tau,k,yo,yeq,excitation,time){
  excfunc <- approxfun(time, excitation, rule = 2)
  parms  <- c(tau = tau, k = k, yeq = yeq, excfunc = excfunc)
  xstart = c(S = yo)
  out <-  lsoda(xstart, time, ODE1, parms)
  return(out[,2])
}


# time <- seq(0,20,length.out = 100)
# excitationin <- c(rep(0,25),rep(1,25),rep(0,50))
# test <- solution_ODE2(5,0.5,1,0,0,excitation = excitationin,time = time)
#plot(time,test)


Generate_simu_order2_simple = function(period,amortfact,k,Npoint,noise,Nindiv){
  data_simu <- data.table(time = rep(1:Npoint,Nindiv),excitation = rep(rep(0,Npoint),Nindiv), ID = rep(1:Nindiv,each = Npoint))
  data_simu[,signalraw := solution_ODE2(period,amortfact,0,0,1,excitation,time),by = ID]
  data_simu[,signal := signalraw + rnorm(.N,0,noise),by = ID]
  return(data_simu)
}
