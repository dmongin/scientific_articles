The different files here contains the results of [data_management.R](../data_management.R) and [analysis.R](../analysis.R):

- clean_data.RData contains the result of the [data_management.R](../data_management.R) with the tables needed for the analysis. The tables are:
  - **vaccins_res** is the table containing for each age category and EPCI the cuéulated vaccination rate of the considered cumulation before the start of the health pass
  - **fit_res** contains the results of the non-linear regression of the weekly vaccination peaks, for each age category and each EPCI. the .1 variables concern the first peak, and .2 the second one.
  - **wholeFrance** is the shapefile of metropolitan and overseas terrotories, with the overseas territories shiftes near France, for map plotting. **all_bbox** is the shapefile to have the squarre around the overseas territories when plotting the maps.
  - **covariate_tot** is the table with all computed covariates:
     - *epci* is the EPCI identifier
     - *icc* is the EPCI name
     - *area* is the EPCI area
     - *pauverty* is the rate of poverty
     - *overcrowed* is the rate of overcrowded livings
     - *pop_without_secondaryeduc* is the rate of population without secondary education
     - *median_living* is the median living income in keuros
     - *ineqalities* is the inequality of incomes
     - *medecin_g* is the number of General Practicioners in the EPCI
     - *pop_tot* is the total popualtion in 2021
     - *density* is the population density, with the associated categorical variable *density_cat*
     - *Npharma* is the number of pharmacies
     - *medecin_density*, *pharma_density*, *med_pharma_density* are the density of GP, pharmacy and both
     - *Nfemme*, *Nhomme*, *prop_women* are the Number of women, men, and the proportion of women
     - *N_center_tot* and *N_center_notclosed* are the number of vaccination center in total, and the number of vaccination centers not closed at the beginning of the health pass, with the associated minimum distance from each center of EPCI *mindist* *mindist_notclosed*
     - *abstention*, *anti_pass*, *pro_pass* are the percentage of abstention, votes for an anti pass or pro pass candidate during the first round of the presidential election
     - *surmortality_glm*, *surmortality_av* are the estimated death excess (considering averaged 2017:2019 as expected death or using poisson model) at the health pass beginning date, with the corresponding categorical variables *surmortality_av_cat* and *surmortality_glm_cat*.
     - *hosp* *rea* are the number of hospitalisation and ICU for COVID19, and the associated percentage of population *hosp_pc* *rea_pc*.
  - **vaccins** is the initial table of vaccination
  - **admin_corresp** is the table of correspondance between the different administrative levels of French administration
- death_EPCI.RData contains the table **death_info** indicating the Number of all cause deaths per EPCI and date
     
     
  

   
   
   
   
   
   
   

