setwd(dirname(rstudioapi::getActiveDocumentContext()$path))
rm(list = ls())
gc()

library(data.table)
library(stringr)
library(lubridate)
library(sf)

library(rjags) # for analysis of variance
library(futile.logger) # logs
library(mice) # multiple imputations
library(dplyr)
library(spdep) # for Moran
library(spatialreg) # for neighbours

# load data and DM ######


load("./results/clean_data.RData")

covariate_tot <- covariate_tot[epci %in% wholeFrance$epci & !is.na(code_dep) & !is.na(code_reg)]

pop_epci <- admin_corresp[,.(population = sum(as.numeric(pop),na.rm = T)),by = epci]
covariate_tot[pop_epci,on = "epci"]
covariate_tot[pop_epci,population := i.population,on = "epci"]


## define outcome ####
fit_res[contrib_pass.2 > 100,contrib_pass.2 := 100]

fit_res[,contrib_pass := contrib_pass.2 - contrib_pass.1]
fit_res[contrib_pass > 100,contrib_pass := 100]

fit_res[vaccins_res,remaining := 100 - i.vaccination - contrib_pass.1,on = c("classe_age","epci")]
fit_res[remaining < 0,remaining := 0]

fit_res[,contrib_pass_rel := contrib_pass/remaining*100]
fit_res[is.infinite(contrib_pass_rel),contrib_pass_rel := NA]
fit_res[contrib_pass_rel < 0,contrib_pass_rel := 0]
fit_res[contrib_pass_rel > 100,contrib_pass_rel := 100]
fit_res$contrib_pass_rel %>% summary()

######## regression functions #####

### create function


#  functions to calculate the p when applying Rubins rules
fisher_transf <- function(R){
  return(log((1+R)/(1-R))/2)
}

inv_fisher_transf <- function(Q){
  return( (exp(2*Q)-1)/(1+exp(2*Q)) )
}

# main analysis function
# consider data with multiple imputation


analysis_spautolm = function(outcome, # character, outcome variable
                             vars, # covariates
                             catvars, # categorical covariates
                             normalize = F, # normalize
                             datain){ # data
  
  # continuous covariates
  cont_vars <- setdiff(vars,catvars)
  
  # function to analyse only one
  analysis_one_df = function(data){

    data <- na.omit(data)
    
    # coeffs normalization
    if(normalize){
      data[,c(cont_vars,outcome) := lapply(.SD,function(x){
        (x - mean(x))/sd(x)
      }),.SDcols = c(cont_vars,outcome)]
    }
    
    # spatial data with covariates
    tmp_france <- left_join(wholeFrance,
                            data,
                            by = "epci") %>%
      filter(!is.na(get(outcome)))
    
    # reorder with same epci as data
    tmp_france <- tmp_france[match(data$epci,tmp_france$epci),]
    
    # neighbor list
    listw <- nb2listw(poly2nb(tmp_france),
                      zero.policy = TRUE,style = "W")
    
    
    # formula
    formula <- paste0(outcome," ~ ",paste0(vars,collapse = " + "))
    # fit
    fit <- spautolm(formula = formula,
                    data = data, 
                    weights = population,
                    listw = listw,
                    zero.policy = T)
    
    # coeffs
    plouf <- summary(fit)
    fit_res <- plouf$Coef %>%
      as.data.table(keep.rownames = T) %>%
      setNames(c("term","estimate","std","z.value","p.value"))
    
    # predict
    # continuous variabels
    coneffect <- rowSums(sapply(cont_vars,function(var){
      fit_res[term == var,estimate]*data[[var]]
    }))
    
    # vategorical variable effect
    data[,cat_effect := 0]  
    for(var in catvars){
      dfvalues <- fit_res[str_detect(term,var),
                          .(term,cat = str_remove(term,var),var = var,estimate)]
      
      for(i in nrow(dfvalues)){
        data[get(var) == dfvalues[i,cat],cat_effect := cat_effect + dfvalues[i,estimate]]    
      }
    }
    
    predicted <- coneffect + data$cat_effect + fit_res[term == "(Intercept)",estimate]
    
    # https://www.tandfonline.com/doi/full/10.1080/02664760802553000
    # calculate R
    p <- length(vars)
    n <- nrow(data)
    SSE <- sum((data[[outcome]]-  predicted)^2)
    SSR <- sum((predicted -  mean(data[[outcome]]))^2)
    SST <- sum((data[[outcome]]-  mean(data[[outcome]]))^2)
    MSE <- mean((data[[outcome]]-  predicted)^2)
    Ra <- sqrt(1-(p-1)/(n-1)*(SSE/SST)) # adjusted R
    R <- sqrt(SSR/SST)
    
    #residuals
    res <- residuals(fit)

    return(list(fit_res = fit_res,
                Ra = Ra,
                R = R,
                fit = fit,
                lambda = plouf$lambda,
                lambda.p.value = plouf$LR1$p.value,
                res = res))
  }
  
  # loop analysis on each imputation
  # long format
  df_long <- complete(datain,"long") %>% setDT()
  # loop analysis on each imputataion
  tmp_list <- lapply(1:datain$m,function(i){ 
    flog.info("model imputation %d",i)
    data <- df_long[.imp == i]
    analysis_one_df(data = data)
  })
  
  # pooling
  # https://bookdown.org/mwheymans/bookmi/rubins-rules.html
  
  # binded coeffs
  coeff_bind <- lapply(tmp_list, function(x) x$fit_res) %>% rbindlist()
  
  # pooled df
  pooled <- coeff_bind[,.(estimate = mean(estimate),
                          w = (mean(std^2)), # within variance
                          m = .N, # m imputation
                          b = 1/(.N-1)*sum( (estimate-mean(estimate))^2)), # between variance
                       by = term]
  
  pooled[,t := w + (1+1/m)*b] # total variance
  pooled[,riv := (b + b/m)/w] 
  pooled[,lambda := (b + b/m)/t]
  
  # degree of freedom for p
  pooled[,dfold := (m-1)/(lambda^2)]
  n <- nrow(datain$data)
  k <- length(vars)
  pooled[,dfobs := ((n-k)+1)/((n-k)+3)*(n-k)*(1-lambda)]
  pooled[,dfadj := dfold*dfobs/(dfold + dfobs)]
  
  pooled[,wald := (estimate^2)/t]
  
  pooled[,p.value := 1-pt(wald,dfadj)]
  pooled[,CI := qt(0.051,dfadj,lower.tail = F)*sqrt(t)] # pooled ste
  
  # mean residuals
  mean_res <- rowMeans(sapply(tmp_list,function(x) x$res))
  
  # moran of outcome
  # no missing outcome, take first imputation
  data_tmp <- na.omit(df_anova_long[.imp == 1])
  outcome_val <- data_tmp[,get(outcome)]
  tmp_france <- left_join(wholeFrance,
                          data_tmp,
                          by = "epci") %>%
    filter(!is.na(get(outcome)))
  tmp_france <- tmp_france[match( data_tmp$epci,tmp_france$epci),]
  
  Moran_outcome <- moran.test(outcome_val,
                              nb2listw(poly2nb(tmp_france),
                                       zero.policy = TRUE),
                              zero.policy = T)
  # Moran of residuals
  Moran_res <- moran.test(mean_res,
                          nb2listw(poly2nb(tmp_france),
                                   zero.policy = TRUE),
                          zero.policy = T)
  
  # pooling of R
  df_R <- lapply(tmp_list,function(x) data.table(Ra=x$Ra,R=x$R)) %>% rbindlist()
  df_R[,Q := fisher_transf(R)]
  Qmean <- mean(df_R$Q)
  Vw <- 1/(df_anova_long[.imp == 1,.N]-3)
  m <- nrow(df_R)
  Vb <- sum(df_R[,R - mean(R)])/(m-1)
  Vtot <- Vw + Vb + Vb/m
  Qup <- Qmean + 1.96*sqrt(Vtot)
  Qdown <- Qmean - 1.96*sqrt(Vtot)
  
  inv_fisher_transf(Qdown)
  inv_fisher_transf(Qup)

  return(
    list(coeffs = pooled,
         R = inv_fisher_transf(Qmean),
         R_up =  inv_fisher_transf(Qup),
         R_down =   inv_fisher_transf(Qdown),
         Moran_res = Moran_res,
         Moran_outcome = Moran_outcome)
  )
  
}


####### mice #####

covariate_tot[,c("metropole") := lapply(.SD,as.factor),.SDcols = c("metropole")]

# define covariate for before health pass
vac_covars <- c("pauverty","pop_without_secondaryeduc","ineqalities",
                "med_pharma_density","dose_pc_beforepass",
                "mindist_notclosed","abstention","pro_pass",
                "surmortality_av" ,"hosp_pc",
                "density_cat","metropole","prop_women")
# define covariate for after health pass
pass_covars <- c("pauverty","pop_without_secondaryeduc","ineqalities",
                 "med_pharma_density","doses_pc_remaining",
                 "mindist_notclosed","abstention","pro_pass",
                 "surmortality_av" ,"hosp_pc",
                 "density_cat","metropole","prop_women")

# subselect chosen covariates
covariate_tot <- covariate_tot[,.SD,.SDcols = c(union(vac_covars,pass_covars),"epci","population")]
# if noone, then set NA
covariate_tot[population == 0,population := NA]

# imputation
covariate_tot_imp <- mice(covariate_tot,
                          maxit = 10,
                          m = 10)


####### analysis #######


analysis = function(classe_age_chosen){
  
  flog.info("\n------- %s ------- \n",classe_age_chosen)
  
  # create complete imputed dataset
  tmp <- complete(covariate_tot_imp,"long", include = TRUE)
  # I merge regression results and vaccination
  tmp_tot <- Reduce(function(x,y) merge(x,y,all = T,by = "epci"),
                    list(fit_res[classe_age == classe_age_chosen,
                                 .(contrib_pass,
                                   epci = as.character(epci),
                                   contrib_pass_rel,
                                   remaining)],
                         vaccins_res[classe_age == classe_age_chosen,
                                     .(epci = as.character(epci),vaccination)],
                         tmp
                    ))
  tmp_tot <- tmp_tot[!is.na(.imp)]
  setkey(tmp_tot,.imp,.id)
  data_tot <- as.mids(tmp_tot) # transform back to mids
  
  # analysis of vaccination
  flog.info("\n------- vaccination ------- \n")
  vac_analysis <- analysis_spautolm(outcome = "vaccination",
                                    vars =  vac_covars,
                                    catvars = c("density_cat","metropole"),
                                    datain = data_tot,
                                    normalize = F)
  
  flog.info("\n------- vaccination norm ------- \n")
  
  vac_analysis_norm <- analysis_spautolm(outcome = "vaccination",
                                         vars =  vac_covars,
                                         catvars = c("density_cat","metropole"),
                                         datain = data_tot,
                                         normalize = T)

  
  flog.info("\n------- pass rel ------- \n")
  
  pass_analysis2 <- analysis_spautolm(outcome = "contrib_pass_rel",
                                      vars =  pass_covars,
                                      catvars = c("density_cat","metropole"),
                                      datain = data_tot,
                                      normalize = F)
  
  flog.info("\n------- pass rel norm ------- \n")
  
  pass_analysis2_norm <- analysis_spautolm(outcome = "contrib_pass_rel",
                                           vars =  pass_covars,
                                           catvars = c("density_cat","metropole"),
                                           datain = data_tot,
                                           normalize = T)
  
  return(list(age = classe_age_chosen,
              vac_covar = vac_covars,
              # pass_covar = pass_covar,
              vac_analysis_norm = vac_analysis_norm,
              vac_analysis = vac_analysis,
              pass_analysis2 = pass_analysis2,
              pass_analysis2_norm = pass_analysis2_norm)
  )
  
}

# main #####

# Loop for all ages

analysis_res <- lapply(c('20-39','40-64','65-75',"75+",'TOUT_AGE'),function(x) {
  analysis(x)
})


####### decomposition variance #########

# function with JAGs to decompose variance
function_variance = function(outcome,
                             datain){
  data <- na.omit(copy(datain[,.SD,.SDcols = c(outcome,"code_dep","code_reg")]))
  
  model_aov <- 
    paste0(" model
{
 # first level
  for (i in 1:n) {
    ",outcome,"[i] ~ dnorm(mu[dep[i]],sigma^-2)
  }

# second level: dep
for(j in 1:J){
mu[j] ~ dnorm(mu.dep[reg[j]], sigma.dep^-2 )
}

# third level: region
for(k in 1:K){
mu.dep[k] ~ dnorm(mu.reg, sigma.reg^-2 )
}

# Priors
mu.reg ~ dunif(0,100)
sigma ~ dunif(0,100)
sigma.dep ~ dunif(0,100)
sigma.reg ~ dunif(0,100)
}
")
  
  data[,code_dep_num := as.numeric(as.factor(code_dep))]
  data[,code_reg_num := as.numeric(as.factor(code_reg))]
  setkey(data,code_dep_num)
  
  model_data_aov <- c(list(n = nrow(data), 
                           J = data[,uniqueN(code_dep_num)],
                           K = data[,uniqueN(code_reg_num)],
                           dep = data$code_dep_num,
                           reg = data[,code_reg_num[1],by = code_dep_num]$V1),
                      list(data[[outcome]]) %>% setNames(outcome)
  )
  
  model <- jags.model(textConnection(model_aov),
                      data = model_data_aov,
                      n.chains = 2,n.adapt = 500)
  
  update(model, 500)
  
  samples <- coda.samples(model,
                          c("mu.reg","sigma.dep","sigma.reg","sigma"),
                          500)
  fit <- summary(samples)
  sigma <- as.data.table(fit$quantiles[,c("2.5%","50%","97.5%")],keep.rownames = T)
  return(sigma)
}

# function to perform the analysis on the data for a chosen age cat
analysis_of_variance = function(classe_age_chosen){
  flog.info("\n------- %s ------- \n",classe_age_chosen)
  
  # merge all data
  data_tot <- Reduce(function(x,y) merge(x,y,all = T,by = "epci"),
                     list(fit_res[classe_age == classe_age_chosen,
                                  .(contrib_pass,
                                    epci = as.character(epci),
                                    contrib_pass_rel,
                                    remaining)],
                          vaccins_res[classe_age == classe_age_chosen,
                                      .(epci = as.character(epci),vaccination)],
                          admin_corresp[,.(code_dep,code_reg,epci)],
                          covariate_tot
                     ))
  
  # apply variance decomposition
  var_vaccination <- function_variance("vaccination",data_tot)
  var_pass <- function_variance("contrib_pass",data_tot)
  var_pass_rel <- function_variance("contrib_pass_rel",data_tot)
  
  # return results
  return(list(var_vaccination = var_vaccination,
              var_pass = var_pass,
              var_pass_rel = var_pass_rel,
              age = classe_age_chosen))
}


# main ####
analysis_var_res <- lapply(c('20-39','40-64','65-75',"75+",'TOUT_AGE'),function(x) {
  analysis_of_variance(x)
})


# save ####
save(analysis_res,
     fit_res,
     vaccins_res,
     wholeFrance,
     covariate_tot,
     all_bbox,
     analysis_var_res,
     file = paste0("./results/analysis.RData"))

