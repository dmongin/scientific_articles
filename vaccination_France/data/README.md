The present folder contains all the data used for this study.

- The folder [shape_files](shape_files) contains the shapefiles at the EPCI level for French metropolitan and overseas territories. They have been downloded from https://www.collectivites-locales.gouv.fr/institutions/cartographie-des-epci-fiscalite-propre
- The folder [covid](covid) contains the data related to COVID-19 in France, namely:
   - [donnees-de-vaccination-par-epci.csv](./covid/donnees-de-vaccination-par-epci.csv) contains the main outcome data, that is the weekly vaccination per EPCI and age category. The data are been recovered from https://datavaccin-covid.ameli.fr/explore/dataset/donnees-de-vaccination-par-epci/export/
   - [donnees-hospitalieres-covid-19-dep-france.csv](./covid/donnees-hospitalieres-covid-19-dep-france.csv) provides the daily number of hospitalisation per department. The file has been recovered from https://data.opendatasoft.com/explore/dataset/donnees-hospitalieres-covid-19-dep-france%40public/information/?disjunctive.countrycode_iso_3166_1_alpha3&disjunctive.nom_dep_min
   - [flux-total-dep.csv](./covid/flux-total-dep.csv) contains the vaccine dose flux in each department. The data has been recovered from https://www.data.gouv.fr/fr/datasets/donnees-relatives-aux-livraisons-de-vaccins-contre-la-covid-19/ 
   - [centres-vaccination.csv](./covid/centres-vaccination.csv) contains the list of all vaccination center in France. The file has been recovered from https://www.data.gouv.fr/fr/datasets/lieux-de-vaccination-contre-la-covid-19/ 
- The folder [covariate](covariate) caontains the data related to the different covariates used in our analysis:
   - [covariate_insee.csv](./covid/covariate_insee.csv) and  [prop_femme.csv](./covid/prop_femme.csv) contains most of the covariates at the PECI level. These data have been extracted from https://statistiques-locales.insee.fr/#c=indicator&view=map4
   - [presidentielle_2022_resultat_communce_tour1.xlsx](./covid/presidentielle_2022_resultat_communce_tour1.xlsx) is the file containing for each commune the result of the first round of the presidential election of 2022 in France, and has been recovered from https://www.data.gouv.fr/fr/pages/donnees-des-elections/
   - [pharmacie_commune.csv](./covid/pharmacie_commune.csv) is a file containing the number of pharmacy per commune, extracted from https://statistiques-locales.insee.fr/#c=indicator&view=map4
- The folder [correspondances](./correspondances) contains data allowing to make the link between the different codes of different levels of administration:
   - [corresp_tot.csv](./correspondances/corresp_tot.csv) makes the correspondance between communes, EPCI, departmenets and regions recovered from https://statistiques-locales.insee.fr/#c=indicator&view=map4
   - [table_passage_geo2003_geo2021.xlsx](./correspondances/table_passage_geo2003_geo2021.xlsx) indicates code changes between the 2003 and 2021 nomenclature in France (used for the localisation of death from the death files), recovered from https://www.insee.fr/fr/information/5057840.
   - [liste_commune_arrondissements.csv](./correspondances/liste_commune_arrondissements.csv) provides the list of communes, with the districts and associated codes, recovered from https://www.insee.fr/fr/information/2028028.

   
   
   
   
   
   
   

